package mohawk.converter.reverse.testing;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.io.*;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.junit.Test;

import mohawk.global.parser.asasptime.nsa.ASASPTimeNSALexer;
import mohawk.global.parser.asasptime.nsa.ASASPTimeNSAParser;
import mohawk.global.parser.asasptime.sa.ASASPTimeSALexer;
import mohawk.global.parser.asasptime.sa.ASASPTimeSAParser;
import mohawk.global.parser.mohawk.v2.Mohawkv2Lexer;
import mohawk.global.parser.mohawk.v2.Mohawkv2Parser;
import mohawk.global.pieces.MohawkT;

public class RegressionTests {
    public BooleanErrorListener errorListener = new BooleanErrorListener();
    public MohawkT m;

    @Test
    public void simpleMohawkRevervseConversion() throws IOException {
        errorListener.errorFound = false;

        InputStream is = new FileInputStream("data/regression/test01.mohawk");
        ANTLRInputStream input = new ANTLRInputStream(is);

        Mohawkv2Lexer lexer = new Mohawkv2Lexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        Mohawkv2Parser parser = new Mohawkv2Parser(tokens);

        parser.removeErrorListeners();
        parser.addErrorListener(errorListener);
        parser.init();

        assertFalse(errorListener.errorFound);

        m = parser.mohawkT;

        // System.out.println(m.getString(null, null, null));
        // 3 because you don't count this separate administrator
        assertEquals((Integer) 3, m.roleHelper.numberOfRoles());
        assertEquals("role1", m.query._roles.get(0).getName());
    }

    @Test
    public void simpleASASPtimeNSARevervseConversion() throws IOException {
        errorListener.errorFound = false;

        InputStream is = new FileInputStream("data/regression/test02.asasptime.nsa");
        ANTLRInputStream input = new ANTLRInputStream(is);

        ASASPTimeNSALexer lexer = new ASASPTimeNSALexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        ASASPTimeNSAParser parser = new ASASPTimeNSAParser(tokens);

        parser.removeErrorListeners();
        parser.addErrorListener(errorListener);
        parser.init();

        assertFalse(errorListener.errorFound);

        m = parser.mohawkT;

        // System.out.println(m.getString(null, null, null));

        assertEquals((Integer) 15, m.roleHelper.numberOfRoles());
        assertEquals("role3", m.query._roles.get(0).getName());
    }

    @Test
    public void simpleASASPtimeSARevervseConversion() throws IOException {
        errorListener.errorFound = false;

        InputStream is = new FileInputStream("data/regression/test03.asasptime.sa");
        ANTLRInputStream input = new ANTLRInputStream(is);

        ASASPTimeSALexer lexer = new ASASPTimeSALexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        ASASPTimeSAParser parser = new ASASPTimeSAParser(tokens);

        parser.removeErrorListeners();
        parser.addErrorListener(errorListener);
        parser.init();

        assertFalse(errorListener.errorFound);

        m = parser.mohawkT;

        // System.out.println(m.getString(null, null, null));

        assertEquals((Integer) 31, m.roleHelper.numberOfRoles());
        assertEquals("role32", m.query._roles.get(0).getName());
    }
}
