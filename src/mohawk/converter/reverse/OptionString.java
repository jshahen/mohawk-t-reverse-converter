package mohawk.converter.reverse;

public enum OptionString {
    HELP("help"), AUTHORS("authors"), VERSION("version"), CHECKNUSMV("checknusmv"), LOGLEVEL("loglevel"), LOGFILE(
            "logfile"), LOGFOLDER("logfolder"), NOHEADER("noheader"), MAXW("maxw"), LINESTR("linstr"), SPECFILE(
                    "input"), SPECEXT("specext"), BULK("bulk"), FROM_ASASPTIME_NSA(
                            "from_asaptime_nsa"), FROM_ASASPTIME_SA("from_asaptime_sa"), FROM_TROLE(
                                    "from_trole"), FROM_TRULE("from_trule"), FROM_MOHAWK(
                                            "from_mohawk"), FROM_ALL("from_all"), SKIP_OUTPUT("skip_output");

    private String _str;

    private OptionString(String s) {
        _str = s;
    }

    @Override
    public String toString() {
        return _str;
    }

    /** Returns the commandline equivalent with the hyphen and a space following: AUTHORS -> "-authors "
     * 
     * @return */
    public String c() {
        return "-" + _str + " ";
    }

    /** Returns the commandline equivalent with the hyphen and a space following: LOGLEVEL("debug") -> "-loglevel debug
     * "
     * 
     * @param param
     * @return */
    public String c(String param) {
        return "-" + _str + " " + param + " ";
    }
}
